﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Net.Sockets;
namespace Server
{
    class Server
    {
        static string data = string.Empty;

        public static void AcceptListeners()
        {
            byte[] sendBack = new Byte[1024];
            byte[] buffer = new Byte[1024];
            IPHostEntry hostIP = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress localIP = hostIP.AddressList[1];
            IPEndPoint localEndPoint = new IPEndPoint(localIP, 10000);
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                socket.Bind(localEndPoint);
                socket.Listen(100);

                while (true)
                {
                    Socket handle = socket.Accept();
                    data = null;
                    while (true)
                    {
                        int bytesRec = handle.Receive(buffer);
                        data += Encoding.ASCII.GetString(buffer, 0, bytesRec);
                        Console.WriteLine($"Data recieved {data}");

                        sendBack = Encoding.ASCII.GetBytes(data);
                        handle.Send(sendBack);
                        if(data == "quit") { break; }
                        else { data = string.Empty; }
                    }
                    Console.WriteLine("I have now exitted the loop");
                    handle.Shutdown(SocketShutdown.Both);
                    handle.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        static int Main(string[] args)
        {
            AcceptListeners();
            Console.ReadLine();
            return 0;

        }
    }
}
