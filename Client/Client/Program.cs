﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Client
{
    class Client
    {
        public static void StartSending()
        {
            byte[] messageBytes;
            string hostName = Dns.GetHostName();
            StringWriter sw = new StringWriter();
            try
            {
                IPHostEntry hostIP = Dns.GetHostEntry(Dns.GetHostName());
                IPAddress localIP = hostIP.AddressList[1];
                IPEndPoint localEndPoint = new IPEndPoint(localIP, 11000);
                Socket localSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                IPEndPoint serverEndPoint = new IPEndPoint(localIP, 10000);

                localSocket.Bind(localEndPoint);
                try
                {
                    localSocket.Connect(serverEndPoint);
                    while (true)
                    {
                        Console.WriteLine("Type something to send");
                        string message = Console.ReadLine();
                        Message sendThisMessage = new Message(message);
                        messageBytes = ASCIIEncoding.ASCII.GetBytes(sendThisMessage.GetMessage);
                        localSocket.Send(messageBytes);
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine(e);
                }
                localSocket.Shutdown(SocketShutdown.Both);
                localSocket.Close();
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
            
        }


        static int Main(string[] args)
        {
            StartSending();
            Console.ReadLine();
            return 0;
        }
    }
}
