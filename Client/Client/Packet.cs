﻿using System;
using System.IO;
using System.Text;
namespace Client
{
    public class Packet
    {

        string _sendTime;

        public Packet()
        {
            _sendTime = DateTime.Now.ToString();
        }

        public string PacketConstructTime
        {
            get => _sendTime;
        }

    }

    public class Message : Packet
    {
        StringBuilder sb = new StringBuilder();
        const string ENDFILE = "<EOF>";
        string _sendTime;
        public Message(string message)
        {
            _sendTime = DateTime.Now.ToString();
            sb.Append(_sendTime + "\"" + message + "\"" + ENDFILE);
        }

        public string GetMessage
        {
            get => sb.ToString();
        }

        public string MessageSendTime
        {
            get => _sendTime;
        }
    }
}
